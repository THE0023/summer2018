#include "List.h"

void Init(List& L)
{
	L.Head = nullptr;
	L.Tail = nullptr;
}

void Init(List& L, const string& InitValue)
{
	L.Head = L.Tail = new ListItem;
	L.Head->Value = InitValue;
	L.Head->Next = nullptr;
}

void Init(List& L, const string InitValues[], const int N)
{
	Init(L);
	for (int i = 0; i < N; i++)
	{
		Append(L, InitValues[i]);
	}
}

void Init(List& L, const List& Other)
{
	Init(L);
	for (ListItem* p = Other.Head; p != nullptr; p = p->Next)
	{
		Append(L, p->Value);
	}
}

void Prepend(List& L, const string& NewValue)
{
	ListItem* NewItem = new ListItem;
	NewItem->Value = NewValue;
	if (IsEmpty(L))
	{
		NewItem->Next = nullptr;
		L.Tail = NewItem;
	}
	else
	{
		NewItem->Next = L.Head;
	}
	L.Head = NewItem;
}

void Append(List& L, const string& NewValue)
{
	ListItem* NewItem = new ListItem;
	NewItem->Value = NewValue;
	NewItem->Next = nullptr;
	if (IsEmpty(L))
	{
		L.Head = NewItem;
	}
	else
	{
		L.Tail->Next = NewItem;
	}
	L.Tail = NewItem;
}

ListItem* FindPrevious(const List& L, const ListItem *Item)
{
    if(Item == L.Head)
        return nullptr;

    for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		if (p->Next == Item)
		{
			return p;
		}
	}
	return nullptr;
}

void InsertBefore(List& L, ListItem* CurrentItem, const string& NewValue)
{
	if (CurrentItem != nullptr)
	{
		if (CurrentItem == L.Head)
		{
			Prepend(L, NewValue);
		}
		else
		{
			ListItem* NewItem = new ListItem;
			NewItem->Value = NewValue;

			/*for(ListItem *I = L.Head; I != nullptr; I= I->Next)
            {
                if(I->Next == CurrentItem)
                {
                    ListItem *P = I;
                    P->Next = NewItem;
                }
            }*/
            ListItem *P = FindPrevious(L, CurrentItem);
            P->Next = NewItem;
			NewItem->Next = CurrentItem;
		}
	}
}

void InsertAfter(List& L, ListItem* CurrentItem, const string& NewValue)
{
	if (CurrentItem != nullptr)
	{
		if (CurrentItem == L.Tail)
		{
			Append(L, NewValue);
		}
		else
		{
			ListItem* NewItem = new ListItem;
			NewItem->Value = NewValue;
			ListItem* N = CurrentItem->Next;
			NewItem->Next = N;
			CurrentItem->Next = NewItem;
		}
	}
}

void RemoveFirst(List& L)
{
	if (!IsEmpty(L))
	{
		InternalRemove(L, L.Head);
	}
}

void RemoveLast(List& L)
{
	if (!IsEmpty(L))
	{
		InternalRemove(L, L.Tail);
	}
}

void Remove(List& L, const string& ValueToRemove)
{
	ListItem* p = Search(L, ValueToRemove);
	if (p != nullptr)
	{
		InternalRemove(L, p);
	}
}

void RemoveAll(List& L, const string& ValueToRemove)
{
	ListItem* p;
	while ((p = Search(L, ValueToRemove)) != nullptr)
	{
		InternalRemove(L, p);
	}
}


void Remove(List& L, const ListItem* ItemToRemove)
{
	if (ItemToRemove != nullptr)
	{
		InternalRemove(L, ItemToRemove);
	}
}

void Clear(List& L)
{
	while (!IsEmpty(L))
	{
		InternalRemove(L, L.Head);
	}
}

ListItem* Search(const List& L, const string& Value)
{
	for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		if (p->Value == Value)
		{
			return p;
		}
	}
	return nullptr;
}


    /**FUNKCE REVERSE SEARCH NEDAVA PRO JEDNOSMERNY SEZNAM SMYSL**/

/*ListItem* ReverseSearch(const List& L, const string& Value)
{
	for (ListItem* p = L.Tail; p != nullptr; p = p->Prev)
	{
		if (p->Value == Value)
		{
			return p;
		}
	}
	return nullptr;
}*/

bool Contains(const List& L, const string& Value)
{
	return Search(L, Value) != nullptr;
}

bool IsEmpty(const List& L)
{
	return L.Head == nullptr;
}

int Count(const List& L)
{
	int counter = 0;
	for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		counter += 1;
	}
	return counter;
}

void Report(const List& L)
{
	for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		cout << p->Value << "\t";
	}
	cout << endl;
}

void ReportStructure(const List& L)
{
	cout << "L.Head: " << L.Head << endl;
	cout << "L.Tail: " << L.Tail << endl;
	for (ListItem* p = L.Head; p != nullptr; p = p->Next)
	{
		cout << "Item address: " << p << endl;
		cout << "Value: " << p->Value << endl;
		//cout << "Prev: " << p->Prev << endl;
		cout << "Next: " << p->Next << endl;
		cout << endl;
	}
}

void InternalRemove(List& L, const ListItem* ItemToDelete)
{
	if (ItemToDelete == L.Head && ItemToDelete == L.Tail)
	{
		L.Head = nullptr;
		L.Tail = nullptr;
	}
	else
	{
		if (ItemToDelete == L.Head)
		{
			L.Head = L.Head->Next;
			ItemToDelete = nullptr;
			//L.Head->Prev = nullptr;
		}
		else
		{
			if (ItemToDelete == L.Tail)
			{
				//L.Tail = L.Tail->Prev;
				ListItem *C = FindPrevious(L, L.Tail);
				L.Tail = C;
				L.Tail->Next = nullptr;
			}
			else
			{
				ListItem* P = FindPrevious(L, ItemToDelete);
				P->Next = ItemToDelete->Next;
			}
		}
	}
	delete ItemToDelete;
}
